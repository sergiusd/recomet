package main

import (
	"encoding/json"
	"fmt"
)

// incoming

type IncomingMessageAbstractContent struct {
	AuthType string `json:"type"`
}

type IncomingMessageSubscribeContent struct {
	AuthType string `json:"type"`
	Routes []string `json:"routes"`
}

type IncomingMessageUnsubscribeContent struct {
	AuthType string `json:"type"`
	Routes []string `json:"routes"`
}

type IncomingMessage struct {
	Client *Client
	Text []byte
}

func (m *IncomingMessage) Parse() (interface{}, error)  {
	test := IncomingMessageAbstractContent{}
	err := json.Unmarshal([]byte(m.Text), &test)
	if err != nil {
		return nil, err
	}
	switch test.AuthType {
	case "subscribe":
		result := IncomingMessageSubscribeContent{}
		err = json.Unmarshal([]byte(m.Text), &result)
		if err != nil {
			return nil, err
		}
		if len(result.Routes) == 0 {
			return nil, fmt.Errorf("Some required data is empty: %v", m.Text)
		}
		return result, nil
	case "unsubscribe":
		result := IncomingMessageUnsubscribeContent{}
		err = json.Unmarshal([]byte(m.Text), &result)
		if err != nil {
			return nil, err
		}
		if len(result.Routes) == 0 {
			return nil, fmt.Errorf("Some required data is empty: %v", m.Text)
		}
		return result, nil
	default:
		return nil, fmt.Errorf("Unknown type %v", test.AuthType)
	}
}

// outgoing

type OutgoingMessageInContent struct {
	Host    string      `json:"host"`
	Event   string      `json:"event"`
	Message interface{} `json:"message"`
	UserIds []uint      `json:"users"`
}

func GetOutgoingMessageInContent(j []byte) (*OutgoingMessageInContent, error) {
	message := &OutgoingMessageInContent{}
	err := json.Unmarshal(j, message)
	if err != nil {
		return nil, err
	}
	if message.Host == "" || message.Event == "" || len(message.UserIds) == 0 {
		return nil, fmt.Errorf("Some required data is empty: %v", message)
	}
	return message, nil

}

type OutgoingMessageOutContent struct {
	Event string        `json:"event"`
	Message interface{} `json:"message"`
}