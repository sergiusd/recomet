package main

import (
	"time"
	"github.com/gorilla/websocket"
	"net/http"
	"fmt"
	"encoding/json"
)

const (
	pongWait = 36 * time.Hour
	maxMessageSize = 1024
	writeWait = 500 * time.Microsecond
)

var upgrader = websocket.Upgrader{
	ReadBufferSize: 1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// Client

type Client struct {
	di *DI
	hub *Hub
	conn *ClientWS
	Host string
	UserId uint
	Id uint64
}

func serveClient(hub *Hub, w http.ResponseWriter, r *http.Request, di *DI) {
	di.Logger.Debug("Request " + r.URL.Path)

	authData, err := parseAuthUrl(r.URL.Path, di)
	if err != nil {
		di.Logger.Error(err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}

	userId, err := authData.sendAuthRequestAndGetUserId()
	if err != nil {
		di.Logger.Error(err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		di.Logger.Error(err.Error())
		return
	}

	clientWS := ClientWS(*conn)
	client := &Client{di: di, hub: hub, conn: &clientWS, Host: authData.Host, UserId: userId, Id: authData.Id}
	client.hub.register <- client

	go client.readPump()
}

func (c *Client) readPump() {
	conn := websocket.Conn(*c.conn)
	defer func() {
		c.hub.unregister <- c
	}()
	conn.SetReadLimit(maxMessageSize)
	conn.SetReadDeadline(time.Now().Add(pongWait))
	conn.SetPongHandler(func(string) error { conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				c.di.Logger.Error(err.Error())
			}
			break
		}
		c.hub.incoming <- &IncomingMessage{Client: c, Text: message}
	}
}

func (c *Client) String() string {
	return fmt.Sprintf("host: %v, userId: %v, id: %v", c.Host, c.UserId, c.Id)
}

// ClientWS

type ClientWS websocket.Conn

func (cws *ClientWS) writeMessage(message *OutgoingMessageOutContent) error {
	payload, err := json.Marshal(message)
	if err != nil {
		return fmt.Errorf("Cant encode json:", message)
	}
	ws := websocket.Conn(*cws)
	ws.SetWriteDeadline(time.Now().Add(writeWait))
	w, err := ws.NextWriter(websocket.TextMessage)
	if err != nil {
		return fmt.Errorf("Cant open websocket writer:", err)
	}
	defer w.Close()
	w.Write(payload)
	return nil
}

func (cws *ClientWS) Close() error {
	conn := websocket.Conn(*cws)
	return conn.Close()
}