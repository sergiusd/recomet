package main

import (
	"strings"
	"fmt"
	"strconv"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"go.uber.org/zap"
)

type AuthData struct {
	di *DI
	authType string
	key string
	Host string
	Id uint64
}

func parseAuthUrl(url string, di *DI) (data *AuthData, err error) {
	items := strings.Split(url, "/")
	if len(items) != 6 || (items[2] != "cookie" && items[2] != "auth2") {
		return nil, fmt.Errorf("Url not correct: %s, %v, %v", url, len(items), items[2])
	}
	id0, err := strconv.ParseInt(items[5], 10, 64)
	if err != nil {
		di.Logger.Error("Cannot parse auth id (must be integer): " + err.Error(), zap.String("url", url))
	}
	return &AuthData{di: di, authType: items[2], Host: items[3], key: items[4], Id: uint64(id0)}, nil
}

func (a *AuthData)sendAuthRequestAndGetUserId() (uint, error) {
	url := a.di.Config.ApiScheme + "://" + a.Host

	switch a.authType {
	case "cookie":
		url += a.di.Config.ApiCookieAuthUrl
	case "oauth2":
		url += a.di.Config.ApiOauth2AuthUrl
	default:
		return 0, fmt.Errorf("Unknown auth type %s", a.authType)
	}

	var client = &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return 0, err
	}

	switch a.authType {
	case "cookie":
		cookie := http.Cookie{Name: a.di.Config.ApiCookieName, Value: a.key}
		req.AddCookie(&cookie)
	case "oauth2":
		req.Header.Set(a.di.Config.ApiOauth2Name, a.key)
	}
	resp, err := client.Do(req)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return 0, fmt.Errorf("Auth response status code %v for %v", resp.StatusCode, url)
	}

	ret, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}

	var result map[string]interface{}
	err = json.Unmarshal([]byte(ret), &result)
	if err != nil {
		return 0, err
	}

	var userId uint
	switch result["userId"].(type) {
	case string:
		userIdTmp, err := strconv.Atoi(result["userId"].(string))
		if err != nil {
			return 0, fmt.Errorf("Cant convert string userId '%v' to int: %v", result["userId"], err)
		}
		userId = uint(userIdTmp)
	case float64:
		userId = uint(result["userId"].(float64))
	default:
		return 0, fmt.Errorf("Cant parse auth json response: %T, %v", result["userId"], result)
	}

	return userId, nil
}