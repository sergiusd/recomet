package main

import (
	"github.com/gomodule/redigo/redis"
	"strconv"
	"strings"
	"fmt"
	"net"
	"sync"
)

/**
 * Данные хранятся в таком формате
 *
 * MapKey
 * m_${account}_${id}: ${user}  - account+id: user   # для определения юзера по данным из запроса
 *
 * UserKey
 * u_${account}_${user}:        - account+user: ids  # для получения списка сессий юзера
 *     ${id1}
 *     ${id2}
 *     ...
 *
 * IdKey
 * i_${account}_${id}:          - account+id: routes # для получения списка роутов для сессии
 *     ${route1}
 *     ${route2}
 *     ...
 */

type Storage struct {
	sync.Mutex
	di *DI
	reconnect *chan bool
	redisConn *redis.Conn
}

// Новое очищенное хранилище
func NewStorage(reconnect *chan bool, di *DI) (*Storage, error) {
	storage := &Storage{reconnect: reconnect, di: di}
	// connect
	err := storage.doConnect()
	if err != nil {
		return nil, err
	}
	// clean
	err = storage.clear(di.Config.RedisFlushAllAlias)
	if err != nil {
		return nil, fmt.Errorf("Cannot clean redis: %v", err)
	}
	return storage, nil
}

// Подключение
func (s *Storage) doConnect() error {
	redisConn, err := redis.Dial(s.di.Config.RedisProtocol, s.di.Config.RedisHostPort)
	if err != nil {
		return fmt.Errorf("Cannot connect to redis: %v", err)
	}
	s.di.Logger.Info("Redis " + s.di.Config.RedisProtocol + "://" + s.di.Config.RedisHostPort + " connected...")
	s.redisConn = &redisConn
	return nil
}

// Переподключение
func (s *Storage) doReconnect() error {
	(*s.redisConn).Close()
	s.redisConn = nil
	err := s.doConnect()
	if err == nil {
		*s.reconnect <- true
	}
	return err
}

// Выполняем команду, если сетевые проблемы, то реконнект и выполняем еще раз
func (s *Storage) do(commandName string, args ...interface{}) (interface{}, error) {
	if s.redisConn ==  nil {
		return nil, fmt.Errorf("Redis disconnected")
	}
	firstAttemp := true
	s.Lock()
TryAgian:
	reply, err := (*s.redisConn).Do(commandName, args...)
	if firstAttemp && err != nil {
		if _, ok := err.(*net.OpError); ok {
			err = s.doReconnect()
			if err == nil {
				firstAttemp = false
				goto TryAgian
			}
		}
	}
	s.Unlock()
	return reply, err
}

// Добавляет запись о сессии юзера
func (s *Storage) SetUserById(host string, userId uint, id uint64) error {
	_, err := s.do("SET", s.getMapKey(host, id), userId)
	if err != nil {
		return err
	}
	_, err = s.do("SADD", s.getUserKey(host, userId), id)
	if err != nil {
		return err
	}
	return nil
}

// Добавляет новые роуты для сессии юзера
func (s *Storage) AddRoutesById(host string, id uint64, routes []string) error {
	args := make([]interface{}, len(routes)+1)
	args[0] = s.getIdKey(host, id)
	for i, route := range routes {
		args[i+1] = route
	}
	_, err := s.do("SADD", args...)
	if err != nil {
		return err
	}
	return nil
}

// Удаляет роут из сессии юзера
func (s *Storage) DelRoutesById(host string, id uint64, routes []string) error {
	args := make([]interface{}, len(routes)+1)
	args[0] = s.getIdKey(host, id)
	for i, route := range routes {
		args[i+1] = route
	}
	_, err := s.do("SREM", args...)
	if err != nil {
		return err
	}
	return nil
}

// Удаляет сессию юзера
func (s *Storage) DropSessionById(host string, id uint64) error {
	mapKey := s.getMapKey(host, id)
	data, err := s.do("GET", mapKey)
	if err != nil {
		return err
	}
	// на случай когда после перезапуска редиса пустое хранилище
	if data == nil {
		return nil
	}
	dataBytes, ok := data.([]byte)
	if !ok {
		return fmt.Errorf("Cannot convert data to byte %T, %v", data, data)
	}
	userId, err := strconv.Atoi(string(dataBytes))
	if err != nil {
		return err
	}

	_, err = s.do("DEL", mapKey)
	if err != nil {
		return err
	}
	_, err = s.do("DEL", s.getIdKey(host, id))
	if err != nil {
		return err
	}
	_, err = s.do("SREM", s.getUserKey(host, uint(userId)), id)
	if err != nil {
		return err
	}
	return nil
}

/**
 * Возвращает данные по роутам пользователя
 *
 * [
 *   SESSION_ID_1 => [ROUTE_1 => true, ROUTE_2 => true, ..., ROUTE_N => true],
 *   ...
 * ]
 */
func (s *Storage) GetIdsAndRoutesByUser(host string, userId uint) (map[uint64]map[string]bool, error) {
	data, err := s.do("SMEMBERS", s.getUserKey(host, userId))
	if err != nil {
		return nil, err
	}
	ids := data.([]interface{})
	ret := make(map[uint64]map[string]bool, 0)
	for _, id0 := range ids {
		id1, err := strconv.ParseInt(string(id0.([]byte)), 10, 64)
		if err != nil {
			return nil, fmt.Errorf("Cant convert to uint %T, %v: %v", id0, id0, err)
		}
		id := uint64(id1)
		data, err = s.do("SMEMBERS", s.getIdKey(host, id))
		if err != nil {
			return nil, err
		}
		routes, ok := data.([]interface{})
		if !ok {
			return nil, fmt.Errorf("Cant convert to []interface %T, %v: %v", routes, routes, err)
		}
		for _, route := range routes {
			routeStr := string(route.([]byte))
			if _, ok := ret[id]; !ok {
				ret[id] = make(map[string]bool, 1)
			}
			ret[id][routeStr] = true
		}
	}
	return ret, nil
}

// Возвращает массив с идентификаторами юзеров аккаунта
func (s *Storage) GetUsersByAccount(host string) ([]uint, error) {
	data, err := s.do("KEYS", s.getUserKeyPatternByAccount(host))
	if err != nil {
		return nil, err
	}
	keys := data.([]interface{})
	ret := make([]uint, 0)
	for _, key := range keys {
		userId, err := s.getUserFromUserKey(string(key.([]byte)))
		if err != nil {
			return nil, err
		}
		ret = append(ret, userId)
	}
	return ret, nil
}

// Чистит данные в хранилище подписок
func (s *Storage) clear(alias string) error {
	if alias == "" {
		alias = "FLUSHALL"
	}
	_, err := s.do(alias)
	if err != nil {
		return err
	}
	return nil
}

func (s *Storage) getMapKey(host string, id uint64) string {
	return "m_" + host + "_" + strconv.FormatUint(id, 10)
}

func (s *Storage) getIdKey(host string, id uint64) string {
	return "i_" + host + "_" + strconv.FormatUint(id, 10)
}

func (s *Storage) getUserKey(host string, userId uint) string {
	return "u_" + host + "_" + strconv.Itoa(int(userId))
}

func (s *Storage) getUserKeyPatternByAccount(host string) string {
	return "u_" + host + "_*"
}

func (s *Storage) getUserFromUserKey(key string) (uint, error) {
	items := strings.Split(key, "_")
	if len(items) == 0 {
		return 0, fmt.Errorf("Zero length split string of", key)
	}
	last := items[len(items)-1]
	ret, err := strconv.Atoi(last)
	if err != nil {
		return 0, err
	}
	return uint(ret), nil
}