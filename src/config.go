package main

import (
	"os"
	"encoding/json"
	"reflect"
	"errors"
)

type Configuration struct {
	Listen string             `json:"listen"`

	RedisProtocol string      `json:"redisProtocol"`
	RedisHostPort string      `json:"redisHostPort"`      // хост редиса для хранения ключей
	RedisFlushAllAlias string `json:"redisFlushAllAlias"` // алиас для команды очистки хранилища

	PubSubProtocol string     `json:"pubSubProtocol"`
	PubSubHostPort string     `json:"pubSubHostPort"`     // хост редиса для pubsub
	PubSubChannel string      `json:"pubSubChannel"`      // имя канала для pubsub

	ApiScheme string          `json:"apiScheme"`          // http, https
	ApiCookieAuthUrl string   `json:"apiCookieAuthUrl"`   // адрес для запроса авторизации
	ApiCookieName string      `json:"apiCookieName"`      // имя куки, в которой хранится идентификатор сессии
	ApiOauth2AuthUrl string   `json:"apiOauth2AuthUrl"`   // адрес для запроса авторизации
	ApiOauth2Name string      `json:"apiOauth2Name"`      // заголовок, в которой хранится токен доступа

	LogLevel string           `json:"logLevel"`           // debug, info, warning, error
}

func (c *Configuration) load(path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&c)
	if err != nil {
		return err
	}
	val := reflect.ValueOf(c).Elem()
	for i := 0; i < val.NumField(); i++ {
		if v := val.Field(i); v.String() == "" {
			return errors.New("Config param '" + val.Type().Field(i).Name + "' is empty")
		}
	}
	return nil
}