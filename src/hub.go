package main

import (
	"go.uber.org/zap"
	"fmt"
)

type Hub struct {
	di *DI
	storage *Storage
	register chan *Client
	unregister chan *Client
	incoming chan *IncomingMessage
	outgoing chan *OutgoingMessageInContent
	reconnect *chan bool
	clients map[uint64]*Client
}

func newHub(storage *Storage, reconnect *chan bool, di *DI) *Hub {
	return &Hub{
		di: di,
		storage: storage,
		register: make(chan *Client),
		unregister: make(chan *Client),
		incoming: make(chan *IncomingMessage),
		outgoing: make(chan *OutgoingMessageInContent),
		reconnect: reconnect,
		clients: make(map[uint64]*Client),
	}
}

func (h *Hub) run() {
	for {
		select {
		case <-*h.reconnect:
			h.di.Logger.Warn("Hub reconnect start...")
			go h.closeAllClients()
		case client := <-h.register:
			h.di.Logger.Debug(
				"Registered client",
				zap.String("client", client.String()),
			)
			h.clients[client.Id] = client
			err := h.storage.SetUserById(client.Host, client.UserId, client.Id)
			if err != nil {
				h.di.Logger.Error("Cannot set user", zap.Error(err))
			}
		case client := <-h.unregister:
			h.di.Logger.Debug(
				"Unregistered client",
				zap.String("client", client.String()),
			)
			err := client.conn.Close()
			if err != nil {
				h.di.Logger.Warn("Cannot close web socket", zap.Error(err))
			}
			err = h.storage.DropSessionById(client.Host, client.Id)
			if err != nil {
				h.di.Logger.Error("Cannot drop session", zap.Error(err))
			}
			if _, ok := h.clients[client.Id]; ok {
				delete(h.clients, client.Id)
			}
		case message := <-h.incoming:
			h.di.Logger.Debug(
				"Handle message",
				zap.String("message", string(message.Text[:])),
				zap.String("client", message.Client.String()),
			)
			go h.processMessage(message)
		case message := <-h.outgoing:
			h.di.Logger.Debug(
				"Send message",
				zap.String("message", fmt.Sprintf("%v", message.Message)),
				zap.String("host", message.Host),
				zap.String("event", message.Event),
				zap.Uints("userIds", message.UserIds),
			)
			go h.SendMessage(message)
		}
	}
}

func (h *Hub) closeAllClients()  {
	for _, client := range h.clients {
		h.unregister <- client
	}
	h.di.Logger.Warn("Hub reconnect complete")
}

func (h *Hub) processMessage(m *IncomingMessage) {
	message, err := m.Parse()
	if err != nil {
		h.di.Logger.Error("Cannot parse message: " + err.Error(), zap.String("message", string(m.Text[:])))
	}
	switch message.(type) {
	case IncomingMessageSubscribeContent:
		h.processSubscribeMessage(m.Client, message.(IncomingMessageSubscribeContent))
	case IncomingMessageUnsubscribeContent:
        h.processUnsubscribeMessage(m.Client, message.(IncomingMessageUnsubscribeContent))
	default:
		h.di.Logger.Error("Unknown message type " + fmt.Sprintf("%T", message))
		return
	}
}

func (h *Hub) processSubscribeMessage(client *Client, message IncomingMessageSubscribeContent) {
	h.di.Logger.Info("Subscription", zap.String("host", client.Host), zap.Uint64("id", client.Id), zap.Strings("routes", message.Routes))
	err := h.storage.AddRoutesById(client.Host, client.Id, message.Routes)
	if err != nil {
		h.di.Logger.Error("Subscription error:", zap.Error(err))
	}
}

func (h *Hub) processUnsubscribeMessage(client *Client, message IncomingMessageUnsubscribeContent) {
	h.di.Logger.Info("Unsubscription", zap.String("host", client.Host), zap.Uint64("id", client.Id), zap.Strings("routes", message.Routes))
	err := h.storage.DelRoutesById(client.Host, client.Id, message.Routes)
	if err != nil {
		h.di.Logger.Error("Unsubscription error:", zap.Error(err))
	}
}

func (h *Hub) SendMessage(m *OutgoingMessageInContent) {
	for _, userId := range m.UserIds {
		idsAndRoutes, err := h.storage.GetIdsAndRoutesByUser(m.Host, userId)
		if err != nil {
			h.di.Logger.Error("Error get map id-routes: " + err.Error())
			continue
		}
		for id, routes := range idsAndRoutes {
			client, ok := h.clients[id]
			if !ok {
				continue
			}
			if _, ok := routes[m.Event]; ok {
				client.conn.writeMessage(&OutgoingMessageOutContent{
					Event:   m.Event,
					Message: m.Message,
				})
			}
		}
	}
}