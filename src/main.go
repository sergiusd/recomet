package main

import (
	"net/http"
	"go.uber.org/zap"
	"os"
)

func main() {

	// config
	if len(os.Args) != 2 {
		panic("USAGE: " + os.Args[0] + " /path/to/config.json")
	}
	config := &Configuration{}
	err := config.load(os.Args[1])
	if err != nil {
		panic(err)
	}

	// logger
	logger, err := GetLogger(config)
	defer logger.Sync()
	if err != nil {
		panic(err)
	}

	// di
	di := &DI {
		Config: config,
		Logger: logger,
	}

	// hub
	reconnectChan := make(chan bool)
	storage, err := NewStorage(&reconnectChan, di)
	if err != nil {
		panic(err)
	}
	hub := newHub(storage, &reconnectChan, di)
	go hub.run()

	// pubsub
	go pubSubListener(hub, di)

	// server
	http.HandleFunc("/online/", func(w http.ResponseWriter, r *http.Request) {
		serveOnline(hub, w, r, di)
	})
	http.HandleFunc("/auth/", func(w http.ResponseWriter, r *http.Request) {
		serveClient(hub, w, r, di)
	})

	logger.Info("Server started...")
	err = http.ListenAndServe(di.Config.Listen, nil)
	if err != nil {
		logger.Error("ListenAndServe: ", zap.Error(err))
	}
}
