package main

import "go.uber.org/zap"

type DI struct {
	Logger *zap.Logger
	Config *Configuration
}