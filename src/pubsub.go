package main

import (
	"github.com/gomodule/redigo/redis"
	"go.uber.org/zap"
	"fmt"
	"time"
	"strconv"
)

const (
	PubSubReconnectTimeoutSecond = 30
)

func pubSubConnect(di *DI) (redis.PubSubConn, error) {
	redisConn, err := redis.Dial(di.Config.PubSubProtocol, di.Config.PubSubHostPort)
	if err != nil {
		return redis.PubSubConn{}, fmt.Errorf("Cannot connect to redis: " + err.Error())
	}
	pubSubConn := redis.PubSubConn{Conn: redisConn}
	err = pubSubConn.Subscribe(di.Config.PubSubChannel)
	if err != nil {
		return redis.PubSubConn{}, fmt.Errorf("Cannot redis subscribe: " + err.Error())
	}
	di.Logger.Info("PubSub listener " + di.Config.PubSubProtocol + "://" + di.Config.PubSubHostPort + " for channel " + di.Config.PubSubChannel + " started...")
	return pubSubConn, nil
}

func pubSubListener(hub *Hub, di *DI) {
	for {
		pubSubConn, err := pubSubConnect(di)
		if err != nil {
			di.Logger.Error(err.Error())
			time.Sleep(PubSubReconnectTimeoutSecond * time.Second)
			continue
		}
	ListenLoop:
		for {
			switch v := pubSubConn.Receive().(type) {
			case redis.Message:
				di.Logger.Debug("Receive pubsub message", zap.String("channel", v.Channel), zap.ByteString("payload", v.Data))
				message, err := GetOutgoingMessageInContent(v.Data)
				if err != nil {
					di.Logger.Error("PubSub message parse error: " + err.Error())
				}
				hub.outgoing <- message
			case error:
				di.Logger.Error(v.Error() + "\nWait " + strconv.Itoa(PubSubReconnectTimeoutSecond) + " seconds and reconnect...")
				pubSubConn.Close()
				time.Sleep(PubSubReconnectTimeoutSecond * time.Second)
				break ListenLoop
			}
		}
	}
}