package main

import (
	"net/http"
	"strings"
	"encoding/json"
	"go.uber.org/zap"
)

func serveOnline(hub *Hub, w http.ResponseWriter, r *http.Request, di *DI) {
	di.Logger.Debug("Request " + r.URL.Path)

	items := strings.Split(r.URL.Path, "/")
	if len(items) != 3 {
		w.WriteHeader(http.StatusNotFound)
		di.Logger.Error("Wrong request", zap.String("url", r.URL.Path))
		return
	}

	userIds, err := hub.storage.GetUsersByAccount(items[2])
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		di.Logger.Error(err.Error())
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(userIds)
}