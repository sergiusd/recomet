package main

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func GetLogger(config *Configuration) (*zap.Logger, error) {
	loggerConfig := zap.NewProductionConfig()
	var logLevel zapcore.Level
	switch config.LogLevel {
	case "debug":
		logLevel = zapcore.DebugLevel
	case "info":
		logLevel = zapcore.InfoLevel
	case "warning":
		logLevel = zapcore.WarnLevel
	case "error":
		logLevel = zapcore.ErrorLevel
	default:
		panic("Unknown logLevel " + config.LogLevel)
	}
	loggerConfig.Level = zap.NewAtomicLevelAt(logLevel)
	loggerConfig.Encoding = "console"
	loggerConfig.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	return loggerConfig.Build()
}